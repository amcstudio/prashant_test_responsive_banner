"use strict";
~(function () {

    // JavaScript Document
    //HTML5 Ad Template JS from DoubleClick by Google

    var container;
    var content;
    var bgExt;
    var btnClose;


    var $ = TweenMax,
        tl,
        bgExit = document.getElementById("bgExit"),
        ad = document.getElementById("mainContent");


    //Function confirm if the creative is visible	
    function enablerInitHandler() {
        if (Enabler.isVisible()) {
            startAd();
        } else {
            Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, startAd);
        }
    };

    //Start the creative
    function startAd() {

        //Assign All the elements to the element on the page
        Enabler.setFloatingPixelDimensions(1, 1);

        Enabler.addEventListener(studio.events.StudioEvent.FULLSCREEN_EXPAND_START, expandHandler);
        Enabler.addEventListener(studio.events.StudioEvent.FULLSCREEN_COLLAPSE_START, collapseHandler);
        Enabler.addEventListener(studio.events.StudioEvent.FULLSCREEN_EXPAND_FINISH, expandFinishHandler);
        Enabler.addEventListener(studio.events.StudioEvent.FULLSCREEN_COLLAPSE_FINISH, collapseFinishHandler);
        Enabler.addEventListener(studio.events.StudioEvent.FULLSCREEN_SUPPORT, fullscreenHandler);


        container = document.getElementById('dc_container');
        content = document.getElementById('dc_content');
        bgExt = document.getElementById('dc_background_exit');
        btnClose = document.getElementById('dc_btnClose');

        addListeners();
        Enabler.queryFullscreenSupport();
        playAnim();
    };

    //Add Event Listeners
    function addListeners() {
        bgExt.addEventListener('touchEnd', clickBG, false);
        bgExt.addEventListener('click', clickBG, false);
        btnClose.addEventListener('touchEnd', clickClose, false);
        btnClose.addEventListener('click', clickClose, false);
        // setTimeout(function(){Enabler.requestFullscreenCollapse()},15000)
    };

    function fullscreenHandler() {
        Enabler.requestFullscreenExpand();
    }
    function expandHandler() {
        container.style.display = "block";
        Enabler.finishFullscreenExpand();
    }
    function expandFinishHandler() {

    }
    function collapseHandler() {
        container.style.display = "none";
        Enabler.finishFullscreenCollapse();
    }
    function collapseFinishHandler() {

    }
    //Add exits
    function clickBG() {
        Enabler.exit('HTML5_Background_Clickthrough');
        Enabler.requestFullscreenCollapse();
    };

    function clickClose() {
        Enabler.reportManualClose();
        Enabler.requestFullscreenCollapse();
        Enabler.close();
    }

    //Wait for the content to load to call the start od the ad
    window.init = function () {
        if (Enabler.isInitialized()) {
            enablerInitHandler();
        } else {
            Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
        }
    };

    function playAnim() {
        tl = new TimelineMax({});
       tl.addLabel('frameOne',0)
       tl.to('#copyOne',1,{opacity:1, y:-10, ease:Power2.easeInOut},'frameOne')
       tl.to('#copyTwo',1,{opacity:1, y:-10, ease:Power2.easeInOut},'frameOne+=.5')
       tl.to('#logo',1,{ y:0, ease: Power2.easeInOut},'frameOne')
       tl.to(['#copyOne','#copyTwo'],0.5,{ opacity:0, ease:Power2.easeInOut},'frameOne+=2')

       tl.addLabel('frameTwo')
       tl.to('#copyThree',1,{opacity:1, y:-10, ease:Power2.easeInOut},'frameTwo')
       tl.to('#copyFour',1,{opacity:1, y:-10, ease:Power2.easeInOut},'frameTwo+=0.5')
       tl.to(['#copyThree','#copyFour'],0.5,{ opacity:0, ease: Power2.easeInOut},'frameTwo+=2')
  
        tl.addLabel('frameThree')
        tl.to('#copyFive',1,{opacity:1, y:-10, ease:Power2.easeInOut},'frameThree')
        tl.to('#copySix',1,{opacity:1, y:-10, ease:Power2.easeInOut},'frameThree+=0.5')
        
        tl.to('#laptop',1,{ x: -10, ease:Power2.easeInOut},'frameThree+=0.5')
        tl.to('#cta',1,{ opacity:1, ease:Power2.easeInOut},'frameThree+=0.5')
    }

    function bgExitHandler() {
        window.open(window.clickTag);
    }
})();